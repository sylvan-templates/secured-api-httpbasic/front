let auth_app = angular.module("auth_app", ['ngRoute']);

auth_app.config(function($routeProvider){
    $routeProvider
    .when('/', {
        redirectTo: '/connexion'
    })
    .when('/connexion', {
        templateUrl: 'tpl/connexion.html',
        controller: 'connexionCtrl'
    })
    .when('/registration', {
        templateUrl: 'tpl/registration.html',
        controller: 'registrationCtrl'
    })
    .when('/password/reset', {
        templateUrl: 'tpl/password/reset.html',
        controller: 'resetPasswordCtrl'
    })
    .when('/password/change', {
        templateUrl: 'tpl/password/change.html',
        controller: 'changePasswordCtrl'
    })
})

auth_app.controller('connexionCtrl', function($scope, $http){
    $scope.connexion = function(){
        $http.post('/api/auth/login', $scope.user);
    }
    $scope.user = {};
})


auth_app.controller('registrationCtrl', function($scope, $http){
    $scope.registration = function(){
        $http.post('/api/auth/register', $scope.user);
    }
    $scope.user = {};
})

auth_app.controller('resetPasswordCtrl', function($scope, $http){
    $scope.resetPassword = function(){
        $http.post('/api/auth/password/reset', $scope.user)
        .then(function(resp){
            if(resp.data.success){

            }else{
                $scope.error_message = resp.data.message;
            }
        })
    }
    $scope.user = {};
})


auth_app.controller('changePasswordCtrl', function($scope, $http, $route, $location){
    $scope.changePassword = function(){
        var token = $route.current.params.token;

        if($scope.user.password == $scope.user.confirmation){
            delete $scope.user['confirmation'];
            $scope.user.token = token;
            $http.put('/api/auth/password/change', $scope.user)
            .then(function(resp){
                if(resp.data.success){
                    $location.search({});
                    $location.path('/connexion');
                }
            })
        }else{
            // TODO: t'es une merde
        }        
    }
})
